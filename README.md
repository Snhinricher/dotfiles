# dotfiles

Contains all configuration settings for linux machines

CONFIG LOCATIONS:
zshrc	  ->  ~/.zshrc
i3	  ->  ~/.config/i3/config 
linux.vim ->  ~/.config/nvim/init.vim

COLORSCHEME LOCATIONS:
~/.local/share/konsole/.
/usr/share/konsole/.

NOTES:
  Clone this repo in the ~/.config folder to keep your home folder clean.
  Create symbolic links of the files to their appropriate locations using 
	  ln -s <source> <target>
  The majority of the nvim config came from https://github.com/mootikins/dotfiles
