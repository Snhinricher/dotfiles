"
"            __    __) _____ __     __) 
"           (, )  /   (, /  (, /|  /|   
"              | /      /     / | / |   
"              |/   ___/__ ) /  |/  |_  
"              |  (__ /   (_/   '       
"                            
"
source ~/.config/dotfiles/vim/core.vim
source ~/.config/dotfiles/vim/fancy.vim
source ~/.config/dotfiles/vim/fullPlug.vim
